from Geometry import Segment, Vector, Particle, Wheel
import pytest


def test_constructor():
    a = Vector(0, 0)
    assert (a.x == 0) and (a.y == 0)
    a = Vector(-10, 1)
    assert (a.x == -10) and (a.y == 1)


def test_norm():
    a = Vector(3, 4)
    assert a.norm() == pytest.approx(5)


def test_addition_mul_():
    a = Vector(-2, 4)
    b = Vector(3, 5)
    a.addition_mul_(b, 0)
    assert (a.x == -2) and (a.y == 4)
    a = Vector(-2, 4)
    a.addition_mul_(b, 1)
    assert (a.x == 1) and (a.y == 9)
    a = Vector(-2, 4)
    a.addition_mul_(b, 2)
    assert (a.x == 4) and (a.y == 14)


def test_sum_list():
    forces = [Vector(1, 0), Vector(0, 1), Vector(3, 4)]
    a = Vector.sum_list(forces)
    assert (a.x == 4) and (a.y == 5)


def test_distance_point():
    s = Segment(Vector(-1, 0), Vector(1, 0))
    p = Vector(0, 5)

    assert s.distance_to_point(p) == pytest.approx(5)


def test_wheel_constructor():
    w = Wheel(10, 10, 1, 10, 2)
    assert w.position.x == 10
    assert w.position.y == 10
    assert w.mass == 1
    assert w.radius == 10
    assert w.force == 2


def test_wheel_contact():
    w = Wheel(10, 10, 1, 5, 1)
    s = Segment(Vector(-10, 0), Vector(10, 0))

    assert w.is_contact_segment(s) == False

    s = Segment(Vector(-10, 5), Vector(10, 5))
    assert w.is_contact_segment(s) == True

    s = Segment(Vector(10.9, 10), Vector(13, 10))
    assert w.is_contact_segment(s) == True
    s = Segment(Vector(11.1, 10), Vector(13, 10))
    assert w.is_contact_segment(s) == False


def test_wheel_freefall():
    w = Wheel(10, 10, 1, 5, 1)
    s = Segment(Vector(-1, 0), Vector(1, 0))
    # Gravity vector
    G = Vector(0, -1)
    F = [G, ]
    total_F = Vector.sum_list(F)
    accel = w.accel_from_forces_free(total_F)

    w.update_state_accel(accel, 1)

    assert w.position.x == pytest.approx(10)
    assert w.position.y == pytest.approx(10)

    assert w.velocity.x == pytest.approx(0)
    assert w.velocity.y == pytest.approx(-1)

    w.update_state_accel(accel, 1)

    assert w.position.x == pytest.approx(10)
    assert w.position.y == pytest.approx(9)

    assert w.velocity.x == pytest.approx(0)
    assert w.velocity.y == pytest.approx(-2)


def test_wheel_contact():
    w = Wheel(1, 1, 1, 5, 1)
    s = Segment(Vector(-1, 0), Vector(1, 0))
    # Gravity vector
    G = Vector(0, -1)
    F = [G, ]
    total_F = Vector.sum_list(F)
    accel = w.accel_from_forces_touching(total_F, s)
    w.update_state_accel(accel, 1)

    assert w.position.x == pytest.approx(1)
    assert w.position.y == pytest.approx(1)

    assert w.velocity.x == pytest.approx(1)
    assert w.velocity.y == pytest.approx(0)

    w.update_state_accel(accel, 1)

    assert w.position.x == pytest.approx(2)
    assert w.position.y == pytest.approx(1)

    assert w.velocity.x == pytest.approx(2)
    assert w.velocity.y == pytest.approx(0)


def test_wheel_bounce():
    w = Wheel(1, 2, 1, 5, 1)
    s = Segment(Vector(-1, 0), Vector(1, 0))
    # Gravity vector
    G = Vector(0, -1)
    F = [G, ]
    total_F = Vector.sum_list(F)
    is_touching = w.is_contact_segment(s)
    if is_touching:
        accel = w.accel_from_forces_touching(total_F, s)
    else:
        accel = w.accel_from_forces_free(total_F)
    w.update_state_accel(accel, 1)
    is_touching_after = w.is_contact_segment(s)
    if (is_touching == False) and (is_touching_after == True):
        w.bounce()
