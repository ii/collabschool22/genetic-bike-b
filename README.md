## (Sort of) Genetic (Kind of) Bicycle B


## Description
This projects simulates a mountain bike (/helicopter/UFO) that attempts, with mixed results, to traverse a rocky grassland from left to right. Run `Simulate_and_Plot.py` to see the bike in action.


## Screenshot of the programme working
![Screenshot](images/bike_screenshot.png)