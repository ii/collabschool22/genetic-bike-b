import numpy as np


class Vector():
    """
    Vector for 2D system, (x,y) coordinates
    """

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def norm(self):
        return np.sqrt(self.x**2 + self.y**2)

    def addition(self, v):
        """
        Adds a vector to the caller and return new vector with result
        """
        return Vector(self.x + v.x, self.y + v.y)

    def subtraction(self, v):
        """
        Subtract a vector to the caller and return new vector with result
        """
        return Vector(self.x - v.x, self.y - v.y)

    def addition_mul_(self, v, c):
        """
        Adds a scaled vector to the caller in place.
        """
        self.x += c * v.x
        self.y += c * v.y

    def division_(self, c):
        """
        Scale a vector using division in place.
        """
        self.x /= c
        self.y /= c

    def division(self, c):
        """
        Scale a vector using division and returns a new vector
        """
        x = self.x / c
        y = self.y / c
        return Vector(x, y)

    def multiplication(self, c):
        """
        Scale a vector using multiplication and returns a new vector.
        """
        x = self.x * c
        y = self.y * c

        return Vector(x, y)

    def multiplication_(self, c):
        """
        Scale a vector using multiplication in place.
        """
        self.x *= c
        self.y *= c

    def as_array(self):
        return np.array([self.x, self.y])

    def dotproduct(self, v):
        """
        Dot product of the caller and another vector.
        """
        return self.x * v.x + self.y * v.y

    @property
    def angle(self):
        """
        Angle of the vector respect to the x axis.
        """

        return np.arctan2(self.y, self.x)

    @classmethod
    def sum_list(self, V):
        """
        Sum of all the vector in a list of vectors. Return a new vector.
        """
        total = Vector(0, 0)
        for v in V:
            total.addition_mul_(v, 1)

        return total


class Particle():
    """
    Represents a single point with position, velocity and mass
    """

    def __init__(self, x=0, y=0, vx=0, vy=0, mass=1):
        self.position = Vector(x, y)
        self.velocity = Vector(vx, vy)
        self.mass = mass

    def update_state_accel(self, accel, dt):
        """
        Updates the inner state (position and velocity) using acceleration vector. It modifies the state.
        """
        self.position.addition_mul_(self.velocity, dt)

        self.velocity.addition_mul_(accel, dt)

    def acceleration(self, F):
        """
        Returns the acceleration produced by external forces given on a list.
        """
        total_force = Vector.sum_list(F)
        return total_force.division(self.mass)

    def update_state_force_list(self, F, dt=1.0):
        """

        """
        accel = self.acceleration(F)
        self.update_state_accel(accel, dt=dt)

    # def as_array(self):
    #     return np.array([self.x, self.y])

    def spring_force(self, p0, l0, k):
        """
        Gets the force produced due the interaction with another particle
        p0: another particle
        l0: resting length of spring
        k: constant of spring
        """
        direction = self.position.subtraction(p0.position)
        distance = direction.norm()
        dd = l0 - distance
        direction_normed = direction.division(distance)

        return direction_normed.multiplication(dd * k)


class Segment():
    """
    Represents a Segment of two points defined as vectors.
    """

    def __init__(self, vector1, vector2):
        """
        Constructor. Requires 2 points.
        """
        self.point1 = vector1
        self.point2 = vector2

        self.vector = self.point2.subtraction(self.point1)
        self.length = self.vector.norm()
        self.direction = self.vector.division(self.length)
        # Normal vector through 90deg rotation
        self.normal = Vector(-self.direction.y, self.direction.x)

    @property
    def angle(self):
        """
        Angle of the segment with respect to the x axis.
        """
        return self.vector.angle

    def distance_to_point(self, vector):
        """
        Computes distance from a point (vector) to line.
        """
        x1 = self.point1.x
        x2 = self.point2.x
        y1 = self.point1.y
        y2 = self.point2.y

        distance_num = np.abs((x2 - x1) * (y1 - vector.y) -
                              (x1 - vector.x) * (y2 - y1))

        return distance_num / self.length


class Wheel(Particle):
    """
    Implements a wheel as a particle with radius. The wheel can push itself in the direction 
    of the segment of contact.
    """

    def __init__(self, x, y, vx, vy, mass, r, F):
        """
        x: x coordinate
        y: y coodinate
        M: mass
        r: radius
        F: force (scalar)
        """
        Particle.__init__(self, x, y, vx, vy, mass)
        self.radius = r
        self.force = F

    def is_contact_segment(self, segment, test_points=10, delta=0):
        """
        Check if wheel is in contact with segment using several test points.
        """
        test_points_x = np.linspace(
            segment.point1.x, segment.point2.x, num=test_points, endpoint=False)
        test_points_y = np.linspace(
            segment.point1.y, segment.point2.y, num=test_points, endpoint=False)
        for tx, ty in zip(test_points_x, test_points_y):
            test_point = Vector(tx, ty)
            distance = test_point.subtraction(self.position).norm()
            if distance <= self.radius + delta:
                return True, distance

        return False, np.nan

    def update_inner_force_(self, segment):
        """
        Computes force produced by the wheel and store in the object.
        Modifies state.
        """
        if self.is_contact_segment(segment):
            self.force = segment.vector.multiplication(
                self.acceleration * segment.length)
        else:
            self.force = Vector(0, 0)

    def update_state_force_list_(self, F, dt):
        """
        Calculate total force and update state accordingly.
        """
        total_force = Vector.sum_list(F + self.force)
        accel = self.acceleration(total_force)
        self.update_state_accel(accel, dt=dt)

    def external_force_projections(self, F_external, segment):
        """
        TODO: change name of the function
        Uses external force list F and segment to calculate the projection 
        on the parallel and perpendicular direction of the segment.
        """
        parallel_projection = segment.direction.x * \
            F_external.x + segment.direction.y * F_external.y

        perpendicular = segment.normal
        perpendicular_projection = perpendicular.x * \
            F_external.x + perpendicular.y * F_external.y

        return parallel_projection, perpendicular_projection

    def accel_from_forces_touching(self, F, segment):
        """
        Estimate acceleration using segment and total external force.
        """
        F_parallel, F_perpendicular = self.external_force_projections(
            F, segment)

        F_perpendicular = 0
        a_parallel = (F_parallel + self.force) / self.mass
        a_perpendicular = F_perpendicular / self.mass

        segment_parallel = segment.direction
        segment_perpendicular = segment.normal

        segment_parallel_s = segment_parallel.multiplication(a_parallel)
        segment_perpendicular_s = segment_perpendicular.multiplication(
            a_perpendicular)

        accel = segment_parallel_s.addition(segment_perpendicular_s)

        return accel

    def accel_from_forces_free(self, F):
        accel = F.division(self.mass)
        return accel

    def bounce(self, segment, damping=1.0, delta=0.01):
        """
        Change the direction of velocity respect to the segment
        using damping in the normal direction, and a delta value
        to avoid vibration.
        """
        v_parallel, v_perpendicular = self.external_force_projections(
            self.velocity, segment)

        v_perpendicular = -damping * v_perpendicular
        if np.abs(v_perpendicular) < delta:
            v_perpendicular = 0.0
        # v_perpendicular = -damping * v_perpendicular

        segment_parallel = segment.direction
        segment_perpendicular = segment.normal

        segment_parallel_s = segment_parallel.multiplication(v_parallel)
        segment_perpendicular_s = segment_perpendicular.multiplication(
            v_perpendicular)

        self.velocity = segment_parallel_s.addition(segment_perpendicular_s)

        return segment_parallel_s.addition(segment_perpendicular_s)
