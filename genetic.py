import numpy
import pygad
import Geometry as geom
# from Fitness import *
from BikeSimulation import simulate
import random as rd

# function_inputs = [1,2,2,2,1,1,2,1] #EXAMPLE SOLUTION

SIMULATION_ITERATIONS = 10000

def fitness_func(solution, solution_idx):
    print("solution", solution)
    global generation_count,generation_population_history
    dots = [1,2,3,4]

    # for i,sol in enumerate(solution):
    #     solution[i] = abs(solution[i])+0.5

    dots[0] = geom.Wheel(
        x=solution[0],
        y=solution[1],
        vx=0.01,
        vy=0.01,
        mass=10,
        r=0.5,
        F=500
    )
    dots[1] = geom.Wheel(
        x=solution[2],
        y=solution[3],
        vx=0,
        vy=0,
        mass=10,
        r=0.5,
        F=0
    )

    dots[2] =geom.Wheel(
        x=solution[4],
        y=solution[5],
        vx=0,
        vy=0,
        mass=50,
        r=0.2,
        F=0
    )
    dots[3] =geom.Wheel(
        x=solution[6],
        y=solution[7],
        vx=0,
        vy=0,
        mass=50,
        r=0.2,
        F=0
    )

    history = simulate(
        dots[0].position.x,
        dots[0].position.y,

        dots[1].position.x,
        dots[1].position.y,

        dots[2].position.x,
        dots[2].position.y,

        dots[3].position.x,
        dots[3].position.y,
        iterations=SIMULATION_ITERATIONS)

    generation_population_history.append(history)
    # print(f"generation_population_history: {generation_population_history}")
    # print(f"HISTORY: {history}")
    # x_values = [ a for (a,b) in history ]
    x_values = [ a[1] for a in generation_population_history ]
    xses = []
    for a in x_values:
        # for v in a:
        xses.append(a)
    # if xses == []:
    #     xses = [0.0001]
    max_x = max(xses)

    fitness = max_x
    return fitness

generation_population_history = []

num_generations = 100
num_parents_mating = 8

sol_per_pop = 8
# num_genes = len(function_inputs)
num_genes = 8

init_range_low = 2
init_range_high = 10

parent_selection_type = "rank"
keep_parents = 0

crossover_type = "single_point"

mutation_type = "random"
mutation_percent_genes = 25

fitness_function = fitness_func

def on_fitness(ga_instance, population_fitness):
    # global generation_count
    # generation_count +=1
    # generation_population_history.append([])
    pass

ga_instance = pygad.GA(num_generations=num_generations,
                       num_parents_mating=num_parents_mating,
                       fitness_func=fitness_function,
                       sol_per_pop=sol_per_pop,
                       num_genes=num_genes,
                       init_range_low=init_range_low,
                       init_range_high=init_range_high,
                       parent_selection_type=parent_selection_type,
                       keep_parents=keep_parents,
                       crossover_type=crossover_type,
                       mutation_type=mutation_type,
                       mutation_percent_genes=mutation_percent_genes,
                       on_fitness=on_fitness,
                       # gene_space = [[0.5,5]]*8
                       )

ga_instance.run()

#best solution calls again the fitness funciton
solution, solution_fitness, solution_idx = ga_instance.best_solution()
print("Parameters of the best solution : {solution}".format(solution=solution))
print("Fitness value of the best solution = {solution_fitness}".format(solution_fitness=solution_fitness))

print(solution_idx)
# prediction = numpy.sum(numpy.array(function_inputs)*solution)  # <- what is this doing? To me this just looks like
                                                               # a function that takes sample inputs for some reason
                                                               # and multiplies them by solution????
# print("Predicted output based on the best solution : {prediction}".format(prediction=prediction))
