from Geometry import Wheel, Segment, Vector, Particle
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

####################################
###### GENERATE LEVEL - START ######
####################################

level_length = 1000
b = 5
a = 0.01

np.random.seed(1337)
ys_ground = ((b - a) * np.random.rand(level_length) + a) / 5
xs_ground = np.array(range(0, level_length))

level_segments = {}
for i in range(level_length - 1):
    level_segments[i] = Segment(Vector(xs_ground[i], ys_ground[i]), Vector(
        xs_ground[i + 1], ys_ground[i + 1]))


##################################
###### GENERATE LEVEL - END ######
##################################

# CONSTANTS

# Spring constant
K0 = 1000

# Gravity vectors
g = -10

MAX_ITER = 10000

# SIMULATION


def simulate(x1, y1, x2, y2, x3, y3, x4, y4, iterations):
    """
    Receives 8 values, creates bike, simulate untils it fails
    """
    # Inputs x1, y1, x2, y2, x3, y3, x4, y4
    w0 = Wheel(x1, y1, 0, 0, 10, 0.5, 500)
    w1 = Wheel(x2, y2, 0, 0, 10, 0.5, 0)
    p0 = Wheel(x3, y3, vx=0, vy=0, mass=50, r=0.2, F=0)
    p1 = Wheel(x4, y4, vx=0, vy=0, mass=50, r=0.2, F=0)

    # Bike
    bicycle_parts = [w0, w1, p0, p1]  # wheel 0, wheel 1
    G_list = []  # Gravity force

    # Distances
    distances_initial = {}
    for part0 in bicycle_parts:
        for part1 in bicycle_parts:
            if part1 == part0:
                continue
            distances_initial[(part0, part1)] = part0.position.subtraction(
                part1.position).norm()

    for obj_number, w in enumerate(bicycle_parts):
        G_list.append(Vector(0, g * w.mass))

    for t in range(iterations):
        # print(t)
        for obj_number, w in enumerate(bicycle_parts):
            forces = []
            for part in bicycle_parts:
                if part == w:
                    continue
                forces.append(w.spring_force(
                    part, distances_initial[(w, part)], K0))

            F = [G_list[obj_number], ] + forces
            total_F = Vector.sum_list(F)
            velocity_sign = 1 if w.velocity.x >= 0 else -1
            try:
                s = level_segments[int(
                    w.position.x + w.radius * velocity_sign)]
            except KeyError:
                del bicycle_parts[obj_number]

            exact_ground_level = np.interp(
                w.position.x, xs_ground, ys_ground)

            try:
                is_touching, _ = w.is_contact_segment(s, 100)
            except UnboundLocalError as e:
                print(e)
                print(f"Terminated by running out of parts at frame {t}")
                return (w0.position.x, t)
            # Hacky solution, but if it works, it works!
            if w.position.y - w.radius <= exact_ground_level:
                w.position.y = exact_ground_level + w.radius
                if obj_number in (2, 3):
                    print(f"Terminated by falling at frame {t}")
                    print(w.position.x, w.position.y)
                    print(w0.position.x, w0.position.y)
                    return (w0.position.x, t)
            if is_touching:
                accel = w.accel_from_forces_touching(total_F, s)
            else:
                accel = w.accel_from_forces_free(total_F)
            w.update_state_accel(accel, 0.01)
            is_touching_after, _ = w.is_contact_segment(s, 100)
            if (is_touching == False) and (is_touching_after == True):
                w.bounce(s, 0.2, 0.01)

            # Return distance
    print(f"Terminated by completing level at frame {t}")
    return (w0.position.x, t)

# initial_state = [6, 2, 8, 2, 6, 5, 8, 5]
initial_state = [
    0.33451076, 8.55775757,
    6.16832176, 5.99044301,
    6.8099769,  5.74691697,
    0.34309449, 5.44026835
    ]

initial_state_str = "3.91131145 10.66433124 5.63647065 5.56056906 8.42899656 9.41892094 6.65449049 4.20598152"


initial_state = initial_state_str.split(" ")
print(initial_state)

initial_state = [float(val) for val in initial_state]

## SAW AT LEAST 184 FRAMES, IF GENETICS ARE WORKING, WE SHULD GET AT LEAST THERE
## SAW AT LEAST 239 FRAMES, ALSO 380+

# initial_state = [6, 2, 8, 2, 6, 5, 8, 5]

D = simulate(*initial_state, MAX_ITER)
