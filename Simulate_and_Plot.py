from Geometry import Wheel, Segment, Vector
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

############################
###### GENERATE LEVEL ######
############################

level_length = 1000
b = 5
a = 0.01

np.random.seed(1337)
ys_ground = ((b - a) * np.random.rand(level_length) + a) / 5
xs_ground = np.array(range(0, level_length))

level_segments = {}
for i in range(level_length - 1):
    level_segments[i] = Segment(Vector(xs_ground[i], ys_ground[i]), Vector(
        xs_ground[i + 1], ys_ground[i + 1]))


def getImage(path):
    return OffsetImage(plt.imread(path, format="png"), zoom=.25, alpha=1)


## Configuration suggested by the evolutionary algorithm:
# w0 = Wheel(0.33451076, 8.55775757, 0, 0, 10, 0.5, 500)
# w1 = Wheel(6.16832176, 5.99044301, 0, 0, 10, 0.5, 0)
# p0 = Wheel(x=6.8099769, y=5.74691697, vx=0, vy=0, mass=50, r=0.2, F=0)
# p1 = Wheel(x=0.34309449, y=5.44026835, vx=0, vy=0, mass=50, r=0.2, F=0)

w0 = Wheel(6, 2, 0, 0, 10, 0.5, 1000)
w1 = Wheel(8, 2, 0, 0, 10, 0.5, 0)
p0 = Wheel(x=6.5, y=3.5, vx=0, vy=0, mass=25, r=0.2, F=0)
p1 = Wheel(x=8, y=3.5, vx=0, vy=0, mass=40, r=0.2, F=0)


bicycle_parts = [
    (w0, "images/bike_wheel_rocky_small.png"),
    (w1, "images/bike_wheel_rocky_small.png"),
    (p0, "images/seat_small.png"),
    (p1, "images/handlebars_small.png")
]

# Spring constant
K0 = 3000

# Gravity vectors
g = -7
G_list = []
for obj_number, w in enumerate(bicycle_parts):
    G_list.append(Vector(0, g * w[0].mass))
    print(w[0].mass)

    F = [G_list[obj_number], ]
    total_F = Vector.sum_list(F)

    print(total_F.x)
    print(total_F.y)
print([o.norm() for o in G_list])

# define the figure hierarchy (figure holds axes)
figure = plt.figure()

axes = figure.add_subplot(111, aspect='equal')

xlim = w0.position.x - 10
xlim_step = 20
axes.set_xlim(xlim, xlim + 20)

plt.ylim(a, b * 3)  # Multiplying Y-axis lim by 3 for better visibility

plt.plot(xs_ground, ys_ground, color="#96d483")
plt.fill_between(xs_ground, ys_ground, color="#37ae12")

log_name = "bike_log.txt"
with open(log_name, "w") as f:
    f.write("Object,Frame,Position")


# Set desired lengths of the springs:
distances_initial = {}
for part0 in bicycle_parts:
    for part1 in bicycle_parts:
        if part1[0] == part0[0]:
            continue
        distances_initial[(part0[0], part1[0])] = part0[0].position.subtraction(
            part1[0].position).norm()


print(distances_initial)


def plot_wheel(object, input_image):
    ab = AnnotationBbox(input_image, (object.position.x,
                                      object.position.y), frameon=False)
    axes.add_artist(ab)
    return ab


def animate(i):
    axes.clear()
    plt.plot(xs_ground, ys_ground, color="#96d483")
    plt.fill_between(xs_ground, ys_ground, color="#37ae12")
    plt.title(f"It's been {i} frames!")

    for obj_number, w in enumerate(bicycle_parts):
        # Forces from spring
        forces = []
        for part0 in bicycle_parts:
            
            if part0[0] == w[0]:
                continue

            forces.append(w[0].spring_force(
                part0[0], distances_initial[(w[0], part0[0])], K0))
            plt.plot(
                [part0[0].position.x, w[0].position.x],
                [part0[0].position.y, w[0].position.y],
                )
        F = [G_list[obj_number], ] + forces
        total_F = Vector.sum_list(F)

        velocity_sign = 1 if w[0].velocity.x >= 0 else -1
        
        # Try to retrieve segment object that is underneath the part;
        # if no segment exists at this X coordinate, it must mean that
        # the part is out of bounds of the level. Assume it's dispoable
        # (for lulz and to allow the simulation last longer).
        # If the part being erased is the driving wheel, simulation ends.

        try:
            s = level_segments[int(
                w[0].position.x + w[0].radius * velocity_sign)]
        except KeyError:
            del bicycle_parts[obj_number]

        exact_ground_level = np.interp(w[0].position.x, xs_ground, ys_ground)

        is_touching, _ = w[0].is_contact_segment(s, 100)
        # Hacky solution, but if it works, it works!
        
        if w[0].position.y - w[0].radius <= exact_ground_level:

            # [CURRENTLY COMMENTED OUT TO MAKE IT MORE FUN]
            # If-statement for ending simulation if handles or the seat touch the ground
            # if obj_number in (2,3):
            #     # print("ERROR ERROR")
            #     print(f"\nPart no {obj_number},{i},{w[0].position.x, w[0].position.y}")
            #     print(w0.position.x, w0.position.y)
                # raise KeyError
                # return obj_number, i ,w[0].position.x
            w[0].position.y = exact_ground_level + w[0].radius

        if is_touching:
            accel = w[0].accel_from_forces_touching(total_F, s)
        else:
            accel = w[0].accel_from_forces_free(total_F)
        w[0].update_state_accel(accel, 0.01)
        # print(
        #     f"State {i}:{is_touching}: {w[0].position.x:0.3f}, {w[0].position.y:0.3f}, {w[0].velocity.x:0.3f}, {w[0].velocity.y:0.3f},{accel.x:0.3f}, {accel.y:0.3f}", )
        is_touching_after, dist_closest = w[0].is_contact_segment(s, 100)
        #print(is_touching, is_touching_after, dist_closest)
        if (is_touching == False) and (is_touching_after == True):
            # print("BOUNCE!")
            w[0].bounce(s, 0.2, 0.01)

      # wheel_artist.xybox = (w[0].position.x, w[0].position.y)  # With this method, the annot box eventually disappears, I'm still not sure why.
                                                                 # Seems to be a bug in matplotlib.
        plot_wheel(w[0], getImage(w[1]))

        with open(log_name, "a") as f:
            f.write(f"\nWheel_{obj_number},{i},{w[0].position.x}")

    xlim = w0.position.x-10
    xlim_step = 20
    plt.xlim(xlim, xlim + xlim_step)
    plt.ylim(a, b * 3)  # Multiplying Y-axis lim by 3 for better visibility

    # figure.canvas.draw()


ani = animation.FuncAnimation(figure,
                              animate,
                              interval=150)

plt.show()
