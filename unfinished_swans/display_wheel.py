from Geometry import Wheel, Segment, Vector
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

w = Wheel(0, 10, 10, 1, 1, 2, 1000)
s1 = Segment(Vector(-200, 0), Vector(0, -4))
s2 = Segment(Vector(0, -4), Vector(200, 0))
S = [s1,
     s2,
     ]
# Gravity vector
g = -10
G = Vector(0, g * w.mass)

# define the figure hierarchy (figure holds axes)
figure = plt.figure()
axes = figure.add_subplot(111, aspect='equal')
axes.set_xlim(-200, 200)
axes.set_ylim(-20, 20)

# add a patch to the axis
ball = plt.Circle((w.position.x, w.position.y), radius=w.radius)
axes.add_patch(ball)
x_values = [s.point1.x for s in S] + [S[-1].point2.x]
y_values = [s.point1.y for s in S] + [S[-1].point2.y]

axes.plot(x_values, y_values)

# is_touching = False
# is_touching_before = False
# pushing_segment = None

# is_touching = [False] * len(S)
# is_touching_before = [False] * len(S)


def animate(i):
    # global is_touching, is_touching_before
    # shift the ball's position
    ball.center = (w.position.x, w.position.y)
    F = [G, ]
    total_F = Vector.sum_list(F)
    pushing_segment = None

    # Bouncing behavior
    for j, s in enumerate(S):
        w.is_contact_segment(s)
        if s in w.contact_segment.keys():
            if (w.contact_segment[s][0] == True):
                if (w.contact_segment[s][1] == False):
                    w.bounce_hold_(s, 0.5, 0.01)
                pushing_segment = s
    w.try_bounce_now_()

    if pushing_segment is not None:
        accel = w.accel_from_forces_touching(total_F, pushing_segment)
    else:
        accel = w.accel_from_forces_free(total_F)
    w.update_state_accel(accel, 0.01)

    print(
        f"State {i}: {w.position.x:0.3f}, {w.position.y:0.3f}, {w.velocity.x:0.3f}, {w.velocity.y:0.3f},{accel.x:0.3f}, {accel.y:0.3f}", )
    # is_touching_after, dist_closest = w.is_contact_segment(s1, 100)
    # print(is_touching, is_touching_after, dist_closest)
    # if (is_touching == False) and (is_touching_after == True):
    #    w.bounce(s1, 0.4, 0.2)
    return ball,

# afterwards, switch to zoomable GUI mode


ani = animation.FuncAnimation(figure,
                              animate,
                              np.arange(1, 50),
                              interval=10)

plt.show()
