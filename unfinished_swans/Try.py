import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import Geometry as geom
import random as rd
import scipy.spatial.distance as sc

#parameter
k = 30

#Generate points
dots = []
mass = np.ones(4)
initial_position = np.random.rand(4,2)
initial_velocity = np.random.rand(4,2)

for i in range(2):
    dots.append(geom.Particle(
        x = rd.random(), 
        y= rd.random(), 
        vx= 0.01, 
        vy= 0.01, 
        mass= 40
    )) 
for i in range(2):    
    dots.append(geom.Wheel(
        x = rd.random(),
        y = 0.1 + rd.random(),
        vx= 0.01, 
        vy= 0.01, 
        M = 40,
        r = 0.1,
        F = 10   
        ))

for i in range(4):
    mass[i] = dots[i].mass
    initial_position[i] = dots[i].position.as_array()
    initial_velocity[i] = dots[i].velocity.as_array()

#initial condition
initial_dist = sc.pdist(initial_position, 'euclidean')

#coordinates
xs = initial_position[:,0]
ys = initial_position[:,1]
vxs = initial_velocity[:,0]
vys = initial_velocity[:,1]

#Gravity vector
g = -8.11
G = geom.Vector(0, g)

#floor
s1 = geom.Segment(geom.Vector(0, 0.1), geom.Vector(0.5, 0.05))
s2 = geom.Segment(geom.Vector(0.5, 0.05), geom.Vector(100, 0.1))
S = [s1, 
     s2,
    ]

#figure
fig, ax = plt.subplots()
# axes = fig.add_subplot(111, aspect='equal')
ax.set_xlim(0, 20)
ax.set_ylim(-10, 10)

plot_dots, = plt.plot(xs, ys, 'ob')



ball = plt.Circle((xs[2], ys[2]), radius=dots[2].radius)
ball1 = plt.Circle((xs[3], ys[3]), radius=dots[3].radius)

# add a patch to the axis
ax.add_patch(ball)
ax.add_patch(ball1)
x_values = [s.point1.x for s in S] + [S[-1].point2.x]
y_values = [s.point1.y for s in S] + [S[-1].point2.y]

plt.plot(x_values, y_values)

# Gravity vector
g = -0.01
G = geom.Vector(0, g)


is_touching = False
is_touching_before = False
pushing_segment = None

is_touching = [False] * len(S)
is_touching_before = [False] * len(S)


#simulation
def update(frame):
    #dots[2] is the wheel with the engine
    global xs, ys, vxs, vys 
    global is_touching, is_touching_before
    xs += vxs 
    ys += vys

    for segment in S:
        segment_begin = segment.point1
        segment_end = segment.point2
        minimum_height = min(segment_begin.y, segment_end.y)
        if xs[2] > segment_begin.x and xs[2] < segment_end.x:
            if ys[2] < minimum_height+.2:
                # seat has touched the ground
                vxs[2] += .8

    for i in range(4):
        cx = xs[i]
        cy = ys[i]
        for segment in S:
            segment_begin = segment.point1
            segment_end = segment.point2
            minimum_height = min(segment_begin.y,segment_end.y)
            if cx > segment_begin.x and cx < segment_end.x:
                if cy < minimum_height+.2:
                    # seat has touched the ground
                    vys[i]*=-1*0.5
                    ys[i]+=.4
    # shift the ball's position
   
    ball.center = (xs[2], ys[2])
    ball1.center = (xs[3], ys[3])
    # dots[2].set_position(xs[2],ys[2])
    # dots[3].set_position(xs[3],ys[3])
    
    F = [G, ]
    total_F = geom.Vector.sum_list(F)
    pushing_segment = None

    dir_acum = []
    #ball
    for j, s in enumerate(S):
        is_touching[j] = dots[2].is_contact_segment(s)
        if (is_touching[j] == True) and (is_touching_before[j] == False):
            pass
            # dir_acum.append(dots[2].bounce(s, 0.3, 0.01))
        # Use last found touching segment
        if is_touching[j]:
            pushing_segment = s
        is_touching_before[j] = is_touching[j]
    
    #ball1
    dir_acum1 = []
    for j, s in enumerate(S):
        is_touching[j] = dots[3].is_contact_segment(s)
        if (is_touching[j] == True) and (is_touching_before[j] == False):
            dir_acum1.append(dots[3].bounce(s, 0.3, 0.01))
        # Use last found touching segment
        if is_touching[j]:
            pushing_segment = s
        is_touching_before[j] = is_touching[j]    
    
    ab = [] 
    # did it touch
    if len(dir_acum) > 0:
        new_vel = geom.Vector.sum_list(dir_acum)
        new_vel.division_(len(dir_acum))
        dots[2].velocity = new_vel
        ab = dots[2].velocity.as_array()
        vxs[2] += ab[0]
        vys[2] += ab[1]
    print(ab)

    if len(dir_acum1) > 1:
        new_vel = geom.Vector.sum_list(dir_acum1)
        new_vel.division_(len(dir_acum1))
        dots[3].velocity = new_vel
        ab = dots[2].velocity.as_array()
        vxs[3] += ab[0]
        vys[3] += ab[1]
        
    if pushing_segment is not None:
        accel = dots[2].accel_from_forces_touching(total_F, pushing_segment)
    else:
        accel = dots[2].accel_from_forces_free(total_F)
    print(accel)
    dots[2].update_state_accel(accel, 0.01)
    print(accel)
    ab = accel.as_array()
    print(ab)
    vxs[2] += ab[0]
    vys[2] += ab[1]


    #length and angle
    l = np.random.rand(6,1) 
    cos_theta = np.random.rand(6,3)
    sen_theta = np.random.rand(6,3)

    count = 0 
    for i in range(3):
        count1 = 0
        for j in range(i+1,4):
            l[count] = ((((xs[i]-xs[j])**2)+((ys[i]-ys[j])**2)))**0.5
            cos_theta[count,count1] = abs(xs[i]-xs[j])/l[count]
            sen_theta[count,count1] = abs(ys[i]-ys[j])/l[count]
            count += 1
            count1 += 1     
   

    count = 0
    for i in range(4):
        for j in range(i+1,4):
            
                #acceleration
                a = ((k) * ((l[count]) - initial_dist[count]))
                count += 1

                #velocities components
                if xs[i] < xs[j]:
                    if ys[i] < ys[j]: 
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]
                else: 
                    if ys[i] < ys[j]:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]
    
    #gravity
    vys += -.05

    #friction
    vxs *= 0.5
    vys *= 0.5



    plot_dots.set_data(xs, ys)
   
    return fig, plot_dots


ani = FuncAnimation(
    fig,
    update,
    interval=50,
    repeat=False,
)
plt.show()