
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

#parameters
k = 30
mass = [40, 40, 40, 40]
#m_red = [0,0,0,0,0,0]

#reduced masses
# count =0
# for i in range(4):
#     for j in range(i+1,4):
#         m_red[count] = (mass[i]*mass[j])/(mass[i]+mass[j])
#         count += 1
       
#Points
dots = np.random.rand(4,4)
dots[0] = [0.1, 0.5, 0, 0]
dots[1] = [0.5, 0.9, 0, 0]
dots[2] = [0.5, 0.1, 0, 0]
dots[3] = [0.9, 0.5, 0, 0]

#Coordinates
xs = dots[:,0]
ys = dots[:,1]
vxs = dots[:,2]
vys = dots[:,3]


# lengths
l0 = [0.3, 0.3, 0.4, 0.3, 0.4, 0.3]

#figure
fig, ax = plt.subplots()
plot_dots, = plt.plot(xs, ys, 'ob')

#simulation
def update(frame):
    global xs, ys, vxs, vys 
    xs += vxs 
    ys += vys
    
    #length and angle
    l = np.random.rand(6,1) 
    cos_theta = np.random.rand(6,3)
    sen_theta = np.random.rand(6,3)

    count = 0 
    for i in range(3):
        count1 = 0
        for j in range(i+1,4):
            l[count] = ((((xs[i]-xs[j])**2)+((ys[i]-ys[j])**2)))**0.5
            cos_theta[count,count1] = abs(xs[i]-xs[j])/l[count]
            sen_theta[count,count1] = abs(ys[i]-ys[j])/l[count]
            count += 1
            count1 += 1     
    

    count = 0
    for i in range(4):
        for j in range(i+1,4):
            
                #acceleration
                a = ((k) * ((l[count]) - l0[count]))
                count += 1

                #velocities components
                if xs[i] < xs[j]:
                    if ys[i] < ys[j]: 
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]
                else: 
                    if ys[i] < ys[j]:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]
                # b = plt.plot([xs[i], xs[j]], [ys[i], ys[j]])  
                # del b     

    
    #gravity
    vys += -0.002

    #friction
    vxs *= 0.5
    vys *= 0.5

    #borders
    xfilter = (xs > 1) | (xs < 0) 
    vxs[xfilter] *= -0.98
    yfilter = (ys > 1) | (ys < 0) 
    vys[yfilter] *= -0.98

    plot_dots.set_data(xs, ys)
   
    return plot_dots,

plt.xlim(0,1)
plt.ylim(0,1)


ani = FuncAnimation(
    fig,
    update,
    #frames=40,
    interval=50,
    repeat=False,
)

plt.show()