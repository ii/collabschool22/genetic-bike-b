
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


##################
##################
level_length = 1000
b = 0.5
a = 0.01

np.random.seed(1337)
ys_ground = (b - a) * np.random.rand(level_length) + a
xs_ground = np.array(range(0,level_length))/8

fig, ax = plt.subplots()
plt.ylim(a, b*3)  # Multiplying Y-axis lim by 3 for better visibility

plot_ground, = plt.plot(xs_ground, ys_ground)
xlim = 0
ax.set_xlim(xlim, xlim+1.5)  # For testing purposes, static


xs_dot = xs_ground[0]+0.25
ys_dot = ys_ground[0]+0.5

vx_dot = np.array(0.5)
vy_dot = np.array(-0.5)

plot_test_dot, = plt.plot(xs_dot, ys_dot, 'ob', markersize=12)


#parameters
k = 2
mass = [50, 50, 50, 50]
m_red = [0,0,0,0,0,0]

#reduced masses
count =0
for i in range(4):
    for j in range(i+1,4):
        m_red[count] = (mass[i]*mass[j])/(mass[i]+mass[j])
        count += 1
       
#Points
dots = np.random.rand(4,4)
dots[0] = [0.4, 0.6, 0, 0]
dots[1] = [0.6, 0.6, 0, 0]
dots[2] = [0.7, 0.7, 0, 0]
dots[3] = [0.3, 1.1, 0, 0]

#Coordinates
xs = dots[:,0]
ys = dots[:,1]
vxs = dots[:,2]
vys = dots[:,3]


#Initial lengths
l0 = [0.2, 0.3, 0.2, 0.3, 0.2, 0.2]

#figure
# fig, ax = plt.subplots()
plot_dots, = plt.plot(xs, ys, 'ob')

#simulation
def update(frame):
    global xs_dot, ys_dot, vx_dot, vy_dot, xs_ground, ys_ground, xlim, ys_ground_iterator, x_y_ground_dict, b, xs, ys, vxs, vys 
    xs += vxs 
    ys += vys
    
    #legth and angle
    l = np.random.rand(6,1) 
    cos_theta = np.random.rand(6,3)
    sen_theta = np.random.rand(6,3)

    count = 0 
    for i in range(3):
        count1 = 0
        for j in range(i+1,4):
            l[count] = ((((xs[i]-xs[j])**2)+((ys[i]-ys[j])**2)))**0.5
            cos_theta[count,count1] = abs(xs[i]-xs[j])/l[count]
            sen_theta[count,count1] = abs(ys[i]-ys[j])/l[count]
            count += 1
            count1 += 1     
    

    count = 0
    for i in range(4):
        for j in range(i+1,4):
            
                #acceleration
                a = ((k/m_red[count]) * ((l[count]) - l0[count]))
                count += 1
                    # vxs[i] += -a*cos_theta[i, j-1]
                    # vys[i] += -a*sen_theta[i, j-1]
                    # vxs[j] += a*cos_theta[i, j-1]
                    # vys[j] += a*sen_theta[i, j-1]
                #velocities components
                if xs[i] < xs[j]:
                    if ys[i] < ys[j]: 
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]
                else: 
                    if ys[i] < ys[j]:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]

    
    #gravity
    vys += -0.003

    #friction
    vxs *= 0.98
    vys *= 0.98

    #borders
    xfilter = (xs < 0) 
    vxs[xfilter] *= -0.25
    curr_ground_level = np.interp(xs, xs_ground, ys_ground)


    # if curr_ground_level[0] > np.interp(xs+0.1, xs_ground, ys_ground)[0]:
    yfilter = (ys > b*5) |  (ys < curr_ground_level)
    
    vys[yfilter]
    vys[yfilter] *= -0.25
    
    # elif curr_ground_level[0] < np.interp(xs+0.1, xs_ground, ys_ground)[0]:
        # yfilter = (ys < 0) |  (ys < curr_ground_level)
    
        # vys[yfilter] *= 0.25
    ys[yfilter] += 0.005
    

    plot_dots.set_data(xs, ys)
   
    ##################
    ##################

    xs_dot += vx_dot # time = 1
    
    ys_dot += vy_dot 
    vy_dot -= 0.15
    
    # xlim_step = vx_dot
    # xlim += xlim_step
    # ax.set_xlim(xlim, xlim+xlim_step)

    exact_ground_level = np.interp(xs_dot, xs_ground, ys_ground)
    ax.set_title(f"Current dot level: {ys_dot}\nCurrent ground level: {exact_ground_level}\nExact location: X: {xs_dot}, Y: {ys_dot}")
    # yfilter = (ys_dot > 100) | (ys_dot < curr_ground_level) 
    if (ys_dot > b*5) | (ys_dot < exact_ground_level):  # If touches ground or flies too high ->
        ys_dot = exact_ground_level+0.05
        

    plot_test_dot.set_data(xs_dot, ys_dot)
    return plot_test_dot, plot_ground, plot_dots,

# plt.xlim(0,1)
# plt.ylim(0,1)

ani = FuncAnimation(
    fig,
    update,
    #frames=40,
    interval=50,
    repeat=False,
)

plt.show()




