import random
from Geometry import Point


class Bike:
    def __init__(self, active_wheel_xy, passive_wheel_xy, seat_1_xy, seat_2_xy, spring_constant=300):
        self.active_wheel = Point(
            x=active_wheel_xy[0], y=active_wheel_xy[1], mass=2)
        self.passive_wheel = Point(
            x=passive_wheel_xy[0], y=passive_wheel_xy[1], mass=2)
        self.seat_1 = Point(x=seat_1_xy[0], y=seat_1_xy[1], mass=40)
        self.seat_2 = Point(x=seat_2_xy[0], y=seat_2_xy[1], mass=40)
        self.spring_constant = spring_constant
