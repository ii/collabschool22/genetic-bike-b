from Geometry import Wheel, Segment, Vector
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

####################################
###### GENERATE LEVEL - START ######
####################################

level_length = 100
b = 5
a = 0.01

np.random.seed(1337)
ys_ground = ((b - a) * np.random.rand(level_length) + a) / 5
xs_ground = np.array(range(0, level_length))

level_segments = {}
for i in range(level_length - 1):
    level_segments[i] = Segment(Vector(xs_ground[i], ys_ground[i]), Vector(
        xs_ground[i + 1], ys_ground[i + 1]))


##################################
###### GENERATE LEVEL - END ######
##################################

def getImage(path):
    return OffsetImage(plt.imread(path, format="png"), zoom=.25, alpha=1)


path = "bike_wheel_rocky_small.png"

w = Wheel(6, 5, 0, 0, 1, 0.5, 500)
w1 = Wheel(6, 6, 0, 0, 1, 0.5, 500)


# Gravity vector
g = -50
G = Vector(0, g * w.mass)

# define the figure hierarchy (figure holds axes)
figure = plt.figure()

axes = figure.add_subplot(111, aspect='equal')

xlim = w.position.x - 10
xlim_step = 20
axes.set_xlim(xlim, xlim + 20)

plt.ylim(a, b * 3)  # Multiplying Y-axis lim by 3 for better visibility

plt.plot(xs_ground, ys_ground, color="#96d483")
plt.fill_between(xs_ground, ys_ground, color="#37ae12")
# add a patch to the axis
ball = plt.Circle((w.position.x, w.position.y), radius=w.radius)
ball1 = plt.Circle((w1.position.x, w1.position.y), radius=w.radius)
# axes.add_patch(ball)

#     axes.plot(x_values, y_values)
# plot_ground, = axes.plot(xs_ground, ys_ground)

wheel_image = getImage(path)
log_name = "wheel_log.txt"
with open(log_name, "w") as f:
    f.write("Object,Frame,Position")


def plot_wheel(input_image):
    ab = AnnotationBbox(
        input_image, (w.position.x, w.position.y), frameon=False)
    axes.add_artist(ab)
    return ab


def plot_wheel1(input_image):
    ab = AnnotationBbox(
        input_image, (w1.position.x, w1.position.y), frameon=False)
    axes.add_artist(ab)
    return ab


#wheel_artist = plot_wheel(input_image=wheel_image)


def animate(i):
    # shift the ball's position
    axes.clear()
    # Plot map on every iteration
    plt.plot(xs_ground, ys_ground, color="#96d483")
    # Color ground
    plt.fill_between(xs_ground, ys_ground, color="#37ae12")

    # Position of the balls
    ball.center = (w.position.x, w.position.y)
    ball1.center = (w1.position.x, w1.position.y)

    F = [G, ]
    total_F = Vector.sum_list(F)

    velocity_sign = 1 if w.velocity.x >= 0 else -1
    # print(velocity_sign)
    # Closest segments
    s = level_segments[int(w.position.x + w.radius * velocity_sign)]
    s1 = level_segments[int(w1.position.x + w1.radius * velocity_sign)]

    # for k, v in level_segments.items():
    #     if k == int(w.position.x):
    #         print(f"Segment {k}")
    # print(w.velocity.x)
    # Exact position
    exact_ground_level = np.interp(w.position.x, xs_ground, ys_ground)
    exact_ground_level1 = np.interp(w1.position.x, xs_ground, ys_ground)
    # Check if wheels are touching a segment
    is_touching, _ = w.is_contact_segment(s, 100)
    is_touching1, _ = w1.is_contact_segment(s, 100)
    # If wheel bottom is below ground then set position above ground
    if w.position.y - w.radius < exact_ground_level:  # Hacky solution, but if it works, it works!
                                                    # (For now, may want to change it later)
        w.position.y = exact_ground_level + w.radius
        # w.bounce(s, 0.4, 0.2)
    #         # is_touching = True

    if w1.position.y - w1.radius < exact_ground_level1:  # Hacky solution, but if it works, it works!
                                                    # (For now, may want to change it later)
        w1.position.y = exact_ground_level1 + w1.radius

    if is_touching:
        accel = w.accel_from_forces_touching(total_F, s)
    else:
        accel = w.accel_from_forces_free(total_F)
    w.update_state_accel(accel, 0.01)

    if is_touching1:
        accel = w1.accel_from_forces_touching(total_F, s)
    else:
        accel = w1.accel_from_forces_free(total_F)
    w1.update_state_accel(accel, 0.01)
    # print(
    #     f"State {i}:{is_touching}: {w.position.x:0.3f}, {w.position.y:0.3f}, {w.velocity.x:0.3f}, {w.velocity.y:0.3f},{accel.x:0.3f}, {accel.y:0.3f}", )
    is_touching_after, dist_closest = w.is_contact_segment(s, 100)
    is_touching_after1, dist_closest = w1.is_contact_segment(s, 100)
    #print(is_touching, is_touching_after, dist_closest)
    if (is_touching == False) and (is_touching_after == True):
        # print("BOUNCE!")
        w.bounce(s, 0.2, 0.01)

    if (is_touching1 == False) and (is_touching_after1 == True):
        # print("BOUNCE!")
        w1.bounce(s, 0.2, 0.01)

    xlim = w.position.x - 10
    xlim_step = 20
    # axes.set_xlim(xlim, xlim+xlim_step)
    plt.xlim(xlim, xlim + xlim_step)
    plt.ylim(a, b * 3)  # Multiplying Y-axis lim by 3 for better visibility

    # wheel_artist.xybox = (w.position.x, w.position.y)  # With this method, the annot box eventually disappears, I'm still not sure why.
    plot_wheel(input_image=wheel_image)
    plot_wheel1(input_image=wheel_image)

    with open(log_name, "a") as f:
        f.write(f"\nWheel_0,{i},{w.position.x}")

    # figure.canvas.draw()


ani = animation.FuncAnimation(figure,
                              animate,
                              interval=15)

plt.show()
