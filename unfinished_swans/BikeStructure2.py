
from calendar import c
from turtle import pos
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import Geometry as geom
import random as rd
import scipy.spatial.distance as sc

#parameter
k = 0.005

#Generate points
dots = []
mass = np.ones(4)
initial_position = np.random.rand(4,2)
for i in range(4):
    dots.append(geom.Particle(
        x = rd.random(), 
        y= rd.random(), 
        vx= rd.random(), 
        vy= rd.random(), 
        mass= 40
    )) 
    mass[i] = dots[i].mass
    initial_position[i] = dots[i].position.as_array()

#initial condition
initial_dist = sc.pdist(initial_position, 'euclidean')

#simulation
def update(dots,POSITION) :

    #positions,velocities and distnces
    position = np.ones((4,2))
    velocity = np.ones((4,2))
    for i in range(4):
        position[i] = dots[i].position.as_array()
        velocity[i] = dots[i].velocity.as_array()
    dist = sc.pdist(position, 'euclidean')

    #angles
    cos_theta = np.random.rand(6,3)
    sen_theta = np.random.rand(6,3)
    count = 0 
    for i in range(3):
        count1 = 0
        for j in range(i+1,4):
            cos_theta[count,count1] = abs(position[i,0]-position[j,0])/dist[i+j-1]
            sen_theta[count,count1] = abs(position[i,1]-position[j,1])/dist[i+j-1]
            count += 1
            count1 += 1 
   
    #dynamic
    count = 0
    for i in range(4):
        for j in range(i+1,4):
            
                #acceleration
                a = ((k) * ((dist[i+j-1]) - initial_dist[i+j-1]))
                count += 1

                #velocities components
                if position[i,0] < position[j,0]:
                    if position[i,1] < position[j,1]: 
                        velocity[i,0] += (a/mass[i])*cos_theta[i, j-1]
                        velocity[i,1] += (a/mass[i])*sen_theta[i, j-1]
                        velocity[j,0] += -(a/mass[j])*cos_theta[i, j-1]
                        velocity[j,1] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        velocity[i,0] += (a/mass[i])*cos_theta[i, j-1]
                        velocity[i,1] += -(a/mass[i])*sen_theta[i, j-1]
                        velocity[j,0] += -(a/mass[j])*cos_theta[i, j-1]
                        velocity[j,1] += (a/mass[j])*sen_theta[i, j-1]
                else: 
                    if position[i,1] < position[j,1]:
                        velocity[i,0] += -(a/mass[i])*cos_theta[i, j-1]
                        velocity[i,1] += (a/mass[i])*sen_theta[i, j-1]
                        velocity[j,0] += (a/mass[j])*cos_theta[i, j-1]
                        velocity[j,1] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        velocity[i,0] += -(a/mass[i])*cos_theta[i, j-1]
                        velocity[i,1] += -(a/mass[i])*sen_theta[i, j-1]
                        velocity[j,0] += (a/mass[j])*cos_theta[i, j-1]
                        velocity[j,1] += (a/mass[j])*sen_theta[i, j-1]
            
    #gravity
    velocity[:,1] += -0.002

    #friction
    velocity[:,0] *= 0.5
    velocity[:,1] *= 0.5

    # #borders
    # xfilter = (velocity[:,0] > 1) | (velocity[:,0] < 0) 
    # velocity[:,0][xfilter] *= -0.98
    # yfilter = (velocity[:,1] > 1) | (velocity[:,1] < 0) 
    # velocity[:,1][yfilter] *= -0.98
    
    POSITION.append(position)
    return POSITION

def simulate(dots):
        POSITION = []
        for i in range(1000):
            update(dots,POSITION)
            last_position = POSITION[-1]
            if check_on_floor(last_position):
                break
        return POSITION

def check_on_floor(last_position):
    


#figure
# fig, ax = plt.subplots()
#plot_dots, = plt.plot(xs, ys, 'ob')


# plt.xlim(0,1)
# plt.ylim(0,1)


# ani = FuncAnimation(
#     fig,
#     update,
#     #frames=40,
#     interval=50,
#     repeat=False,
# )

# plt.show()