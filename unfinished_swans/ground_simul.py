import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

"""
Currently the only thing this does is create a random hilly landscape,
then scroll through it left to right by dynamically updating xlim values.

TODO:
- dynamic scrolling (instead of variable `interval`
it can be done with passing xlim value explicitly)

CHANGELOG:
- raised level floor for visibility
- set random seed for reproducibility
- add tracking a dot that falls on the ground and  across the ground

"""


level_length = 1000
b = 20
a = 10

np.random.seed(1337)
ys_ground = (b - a) * np.random.rand(level_length) + a
xs_ground = np.array(range(0,level_length))

fig, ax = plt.subplots()
plt.ylim(a, b*3)  # Multiplying Y-axis lim by 3 for better visibility

plot_ground, = plt.plot(xs_ground, ys_ground)
xlim = 0

xs_dot = xs_ground[0]+2.25
ys_dot = ys_ground[0]+5

vx_dot = np.array(5)
vy_dot = np.array(-0.5)

plot_test_dot, = plt.plot(xs_dot, ys_dot, 'ob', markersize=12)


def update_ground(frame):
    global xs_dot, ys_dot, vx_dot, vy_dot, xs_ground, ys_ground, xlim, ys_ground_iterator, x_y_ground_dict, b
    xs_dot += vx_dot # time = 1
    
    ys_dot += vy_dot 
    vy_dot -= 0.05
    
    xlim_step = vx_dot
    xlim += xlim_step
    ax.set_xlim(xlim, xlim+xlim_step)

    exact_ground_level = np.interp(xs_dot, xs_ground, ys_ground)
    ax.set_title(f"Current dot level: {ys_dot}\nCurrent ground level: {exact_ground_level}\nExact location: X: {xs_dot}, Y: {ys_dot}")
    # yfilter = (ys_dot > 100) | (ys_dot < curr_ground_level) 
    if (ys_dot > b*5) | (ys_dot < exact_ground_level):  # If touches ground or flies too high ->
        ys_dot = exact_ground_level+0.75
        

    plot_test_dot.set_data(xs_dot, ys_dot)
    return plot_test_dot, plot_ground,


ani = FuncAnimation(
    fig,
    update_ground,
    # frames=10,
    interval=150,
    repeat=False,
)

plt.show()
