from Geometry import Wheel, Segment, Vector
import numpy as np

####################################
###### GENERATE LEVEL - START ######
####################################

level_length = 10000
b = 5
a = 0.01

np.random.seed(1337)
ys_ground = ((b - a) * np.random.rand(level_length) + a)/5
xs_ground = np.array(range(0,level_length))

level_segments = {}
for i in range(level_length-1):
    level_segments[i] = Segment(Vector(xs_ground[i], ys_ground[i]), Vector(xs_ground[i+1], ys_ground[i+1]))

##################################
###### GENERATE LEVEL - END ######
##################################


w = Wheel(6, 5, 5, 0.5, 500)

# Gravity vector
g = -50
G = Vector(0, g * w.mass)


log_name = "wheel_log.txt"

with open(log_name, "w") as f:
    f.write("Object,Frame,Position")

def simulate(i):
    # shift the ball's position
    F = [G, ]
    total_F = Vector.sum_list(F)

    velocity_sign = 1 if w.velocity.x >= 0 else -1
    # print(velocity_sign)
    s = level_segments[int(w.position.x+w.radius*velocity_sign)]
    
    exact_ground_level = np.interp(w.position.x, xs_ground, ys_ground)
    
    is_touching, _ = w.is_contact_segment(s, 100)
    if w.position.y-w.radius < exact_ground_level:  # Hacky solution, but if it works, it works!
                                                    # (For now, may want to change it later)
        w.position.y = exact_ground_level+w.radius

    if is_touching:
        accel = w.accel_from_forces_touching(total_F, s)
    else:
        accel = w.accel_from_forces_free(total_F)
    w.update_state_accel(accel, 0.01)
    is_touching_after, dist_closest = w.is_contact_segment(s, 100)
    if (is_touching == False) and (is_touching_after == True):
        w.bounce(s, 0.2, 0.01)

    with open(log_name, "a") as f:
        f.write(f"\nWheel_0,{i},{w.position.x}")

frame_limit = 10000

for frame in range(0,frame_limit):
    simulate(frame)

print(f"FINISHED SIMULATING {frame_limit} frames.")