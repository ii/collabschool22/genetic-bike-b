import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import Geometry as geom
import random as rd
import scipy.spatial.distance as sc
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

#parameter
k = 2

#Generate points
dots = []
mass = np.ones(4)
initial_position = np.random.rand(4,2)
initial_velocity = np.random.rand(4,2)

for i in range(2):
    dots.append(geom.Particle(
        x = rd.random(), 
        y= rd.random(), 
        vx= 0.01, 
        vy= 0.01, 
        mass= 40
    )) 
for i in range(2):    
    dots.append(geom.Wheel(
        x = rd.random(),
        y = 0.1 + rd.random(),
        vx= 0.01, 
        vy= 0.01, 
        M = 40,
        r = 0.1,
        F = 10   
        ))

for i in range(4):
    mass[i] = dots[i].mass
    initial_position[i] = dots[i].position.as_array()
    initial_velocity[i] = dots[i].velocity.as_array()

#initial condition
initial_dist = sc.pdist(initial_position, 'euclidean')

#coordinates
xs = initial_position[:,0]
ys = initial_position[:,1]
vxs = initial_velocity[:,0]
vys = initial_velocity[:,1]

#FLOOR (wheel_on_level implementation)
level_length = 100
b = 5
a = 0.01
np.random.seed(1337)
ys_ground = ((b - a) * np.random.rand(level_length) + a)/5
xs_ground = np.array(range(0,level_length))
level_segments = {}
for i in range(level_length-1):
    level_segments[i] = geom.Segment(geom.Vector(xs_ground[i], ys_ground[i]), geom.Vector(xs_ground[i+1], ys_ground[i+1]))

#wheel image
def getImage(path):
   return OffsetImage(plt.imread(path, format="png"), zoom=.25, alpha=1)

path = "/Users/marcellocostamagna/Desktop/Genetic Bike B/genetic-bike-b/bike_wheel_rocky_small.png"

#Defining the two wheels previously generated
w = dots[2]
w1 = dots[3]

#figure
fig, ax = plt.subplots()
axes = fig.add_subplot(111, aspect='equal')

#follow the wheel
xlim = dots[2].position.x-10
xlim_step = 20
axes.set_xlim(xlim, xlim+20)
wheel_image = getImage(path)
log_name = "wheel_log.txt"
with open(log_name, "w") as f:
    f.write("Object,Frame,Position")

def plot_wheel(input_image):
    ab = AnnotationBbox(input_image, (w.position.x, w.position.y), frameon=False)
    axes.add_artist(ab)
    return ab

wheel_artist = plot_wheel(input_image=wheel_image)   

#Gravity vector
g = -0.01
G = geom.Vector(0, g)

#floor
s1 = geom.Segment(geom.Vector(0, 0.1), geom.Vector(0.5, 0.05))
s2 = geom.Segment(geom.Vector(0.5, 0.05), geom.Vector(1, 0.1))
S = [s1, 
     s2,
    ]

plt.ylim(a, b*3)  # Multiplying Y-axis lim by 3 for better visibility
plt.plot(xs_ground, ys_ground, color="#96d483")
plt.fill_between(xs_ground, ys_ground, color="#37ae12")

plot_dots, = plt.plot(xs, ys, 'ob')

ball = plt.Circle((xs[2], ys[2]), radius=w.radius)
ball1 = plt.Circle((xs[3], ys[3]), radius=w1.radius)

# Gravity vector
g = -0.01
G = geom.Vector(0, g)

#simulation
def update(frame):
    global xs, ys, vxs, vys 
    
    xs += vxs 
    ys += vys

    axes.clear()
    plt.plot(xs_ground, ys_ground, color="#96d483")
    plt.fill_between(xs_ground, ys_ground, color="#37ae12")
    # shift the ball's position
   
    ball.center = (xs[2], ys[2])
    ball1.center = (xs[3], ys[3])

    F = [G, ]
    total_F = geom.Vector.sum_list(F)

    velocity_sign = 1 if w.velocity.x >= 0 else -1
    # print(velocity_sign)
    s = level_segments[int(w.position.x + w.radius*velocity_sign)]

    exact_ground_level = np.interp(w.position.x, xs_ground, ys_ground)
    
    is_touching, _ = w.is_contact_segment(s, 500)
    if w.position.y-w.radius < exact_ground_level:  # Hacky solution, but if it works, it works!
                                                    # (For now, may want to change it later)
        w.position.y = exact_ground_level+w.radius

    if is_touching:
        accel = w.accel_from_forces_touching(total_F, s)
    else:
        accel = w.accel_from_forces_free(total_F)
    w.update_state_accel(accel, 0.01)
    is_touching_after, dist_closest = w.is_contact_segment(s, 100)
    #print(is_touching, is_touching_after, dist_closest)
    if (is_touching == False) and (is_touching_after == True):
        # print("BOUNCE!")
        w.bounce(s, 0.2, 0.01)
    
    xlim = w.position.x-10
    xlim_step = 20
    # axes.set_xlim(xlim, xlim+xlim_step)
    plt.xlim(xlim, xlim+xlim_step)
    #plt.ylim(a, b*3)  # Multiplying Y-axis lim by 3 for better visibility

    # wheel_artist.xybox = (w.position.x, w.position.y)  # With this method, the annot box eventually disappears, I'm still not sure why.
    plot_wheel(input_image=wheel_image)

    # with open(log_name, "a") as f:
    #     f.write(f"\nWheel_0,{i},{w.position.x}")



    #length and angle
    l = np.random.rand(6,1) 
    cos_theta = np.random.rand(6,3)
    sen_theta = np.random.rand(6,3)

    count = 0 
    for i in range(3):
        count1 = 0
        for j in range(i+1,4):
            l[count] = ((((xs[i]-xs[j])**2)+((ys[i]-ys[j])**2)))**0.5
            cos_theta[count,count1] = abs(xs[i]-xs[j])/l[count]
            sen_theta[count,count1] = abs(ys[i]-ys[j])/l[count]
            count += 1
            count1 += 1     
   

    count = 0
    for i in range(4):
        for j in range(i+1,4):
            
                #acceleration
                a = ((k) * ((l[count]) - initial_dist[count]))
                count += 1

                #velocities components
                if xs[i] < xs[j]:
                    if ys[i] < ys[j]: 
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += (a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += -(a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]
                else: 
                    if ys[i] < ys[j]:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += (a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += -(a/mass[j])*sen_theta[i, j-1]
                    else:
                        vxs[i] += -(a/mass[i])*cos_theta[i, j-1]
                        vys[i] += -(a/mass[i])*sen_theta[i, j-1]
                        vxs[j] += (a/mass[j])*cos_theta[i, j-1]
                        vys[j] += (a/mass[j])*sen_theta[i, j-1]
    
    #gravity
    vys += -0.001

    #friction
    vxs *= 0.9
    vys *= 0.9

    plot_dots.set_data(xs, ys)
   
    return fig, plot_dots

plt.xlim(0,1)
plt.ylim(0,1)

ani = FuncAnimation(
    fig,
    update,
    interval=50,
    repeat=False,
)
plt.show()